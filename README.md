# Auditor
* This tool will assist with the auditing process for integration tests.
* This tool has both the React server for Front-End as well as a NodeJS server running the web server.
## Starting
### Prerequisites
* NodeJS for server
* SQL
* SQL Server Management Studios
* Visual Studio Code (not mandatory)
### Installing
* Install Visual Studio Code (not mandatory)
* Run "npm install" in the root directory to install node dependencies for the NodeJS server.
* CD into the auditor folder and run "npm install" to load all React packages used for this project.
* Using SQL server, create a new database called Auditor.
* Execute the queries saved within the SQL folder of the project.
* Modify the DB Config file to use your SQL credentials.
### Running
* Within the QA_TOOL folder, run "npm run dev". We have the package concurrently that will spin up both the NodeJS server as well as the React server.
## Documentation
* A complete walkthrough of the application can be found in the Auditor.docx!
### Index.js (Root Directory)
* Contains all API endpoints for the React Application.
* Endpoints for storing/retrieving data from SQL is using local temp database. Will eventually need to connect to Backend API Database.
* Contains route for production build of the Auditor. Uncomment route once ready to deploy.
### SQL (Folder)
* Contains SQL Scripts for setting up mock database.
### config/dev.js
* Contains config object for SQL authentication. Replace data with your login credentials.
# auditor/src
## actions (Folder)
### types.js
* Responsible for declaring new providers that are going to be used throughout the application.
* These providers will then be assigned in index.js.
### index.js
* Implements our providers created in types.js.
* Current provider is FETCH_USER. Which is responsible for targeting Node.JS endpoint for current user logged in.
* If a user is logged in, it returns true and all pages become accessible in App.js. User data is also accessible from auth object.
* If a user is not logged in, it returns false and most pages are restricted in App.js.
## reducers (Folder)
### authReducer.js
* Creates the callback function for the FETCH_USER provider.
* Returns auth (user) object if user is cuurently logged in.
* Returns false if no user is authenticated.
* Default state is null. And is returned while still checking for authentication.
### index.js
* Binds all providers to object name.
* Object name used in other js files to bind to their specific provider.
## Root src (Folder)
### Header.js
* Class component responsible for generating the UI header for the application.
* Maps to auth provider, responsible for determining auth level and access.
### App.js
* Sets up routes for the Front-End application.
* Routes are determined based off of authentication.
### Login.js
* Generates form for logging in a new user.
* Components are used to valifate data passed into forms.
* Submit routes to Node.JS endpoint to check credentials against user table.
* Endpoint routes back to login if user doesn't exist/isn't found.
* Login page is routed to first if no user is logged in.
### Register.js
* Generates the form for creating a new user.
* Components are used to validate data passed into forms.
* Submit routes to Node.JS endpoint to store credentials to the DB.
* All users are given default role on creation.
* Admin rights are to be set manually.
### Home.js
* Home page after authentication.
* This page is where you upload your bill to a selected vendor.
* Once a bill us uploaded, user will be redirected to summary page.
* Bill data is yet to be sent to Back End for Auditing.
### Summary.js
* Incomplete at the moment.
* Displays results of the auditing process to show potential savings.
* Can export data returned into CSV File for later use.
### Redirect.js
* Page is displayed as a result of unauthorized access to admin pages.
* Page will redirect user to login page after 5 seconds to allow them to login with an admin account.
### Vendor_List.js
* Admin users are the only ones able to access this page.
* Default users trying to access this page will be greeted with Redirect page.
* Displays current list of vendors stored within the DB.
* User has ability to edit/delete any existing vendor.
* New vendors can be created as well from this page.
### Alter_Vendor.js
* Admin users are the only ones able to access this page.
* Default users trying to access this page will be greeted with Redirect page.
* Generates forms that are used to receive data regarding a vendor.
* Creating a new vendor will render an empty form. Data sent on submit to Node.JS endpoint to store data into DB.
* Editing a vendor will make request to Node.JS server to grab and populate form with existing vendor data.
* Data is then used to update existing vendor.
### *.test.js
* All unit tests created for each js file.
* Main purpose is to ensure that page renders without breaking.
* As well as mocking API calls, form input changes, form submits, etc...