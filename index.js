var express = require('express');
var path = require('path');
var cookieSession = require('cookie-session');
var bcrypt = require('bcrypt');
var bodyParser = require('body-parser');
var keys = require('./config/keys');
var sql = require('mssql');
var app = express();
var busboy = require('connect-busboy');
var fs = require('fs');

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });

app.use(express.static(path.join(__dirname, 'auditor/build')));
app.use(busboy());

// app.get('*', function(req, res) {
//     res.sendFile(path.resolve(__dirname, 'auditor', 'build', 'index.html'));
// });

var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(
    cookieSession({
        maxAge: 30 * 23 * 60 * 60 * 1000,
        keys: [keys.key]
    })
);

app.get('/api', (req, res) => {
    if (req.session != null) {
        req.session = null;
    }
    res.render('/login');
});

app.post('/api/registered', urlencodedParser, (req, res) => {
    var username = req.body.username;
    var password = req.body.password;
    var doesExist = false;
    sql.close();
    sql.connect(keys.config)
    .then(() => {
        var request = new sql.Request();
        request.query('select * from Users', (err, data) => {
            if (err) console.log(err);
            for (var i = 0; i < data.recordset.length; i++) {
                if (data.recordset[i].UserName === username && data.recordset[i].Password === password) {
                    doesExist = true;
                    break;
                }
            }
            if (!doesExist) {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if(err) return err;
                    else{
                        password = hash;
                    }
                    request.query("INSERT INTO Users (UserName, Password, Level) VALUES ('"+username+"', '"+password+"', 'Default')")
                    .then((result) => {
                        console.log('Recordset: ' + result);
                        console.log('Affected: ' + request.rowsAffected);
                    })
                    .catch((err) => {
                        console.log(err)
                    });
                });
            }
            res.redirect('/');
        });
    })
    .catch((err) => {
        console.log(err);
    });
});

app.post('/api/authenticate', urlencodedParser, (req, res) => {
    var username = req.body.username;
    var password = req.body.password;
    let doesExist = false;
    sql.close();
    sql.connect(keys.config)
    .then(() => {
        var request = new sql.Request();
        request.query('select * from dbo.Users', async (err, data) => {
            if (err) console.log(err);
            for (var i = 0; i < data.recordset.length; i++) {
                if (data.recordset[i].UserName === username && bcrypt.compare(password, data.recordset[i].Password)) {
                    doesExist = true;
                    req.session.user = data.recordset[i];
                    break;
                }
            }
            if (doesExist) {
                res.redirect('/home');
            }
            else {
                res.redirect('/');
            }
        });
    })
    .catch((err) => {
        console.log(err);
    });
});

app.get('/api/user', (req, res) => {
    if (typeof req.session.user === 'undefined') {
        console.log('No user is signed into the application!')
    }
    res.send(req.session.user);
});

app.get('/api/logout', (req, res) => {
    req.session = null;
    res.redirect('/');
});

app.get('/api/sprocs', (req, res) => {
    let sprocs =
    {
        "salesForce": [
            {value: '0', label: '0'},
            {value: '1500', label: '1500'},
            {value: '25', label: '25'},
            {value: '333', label: '333'},
            {value: '2', label: '2'},
            {value: '1000', label: '1000'},
            {value: '9000', label: '9000'},
            {value: '400', label: '400'}
        ],
        "integrations": [
            {value: '0', label: '0'},
            {value: '1500', label: '1500'},
            {value: '25', label: '25'},
            {value: '333', label: '333'},
            {value: '2', label: '2'},
            {value: '1000', label: '1000'},
            {value: '9000', label: '9000'},
            {value: '400', label: '400'}
        ],
        "billSetup": [
            {value: '0', label: '0'},
            {value: '1500', label: '1500'},
            {value: '25', label: '25'},
            {value: '333', label: '333'},
            {value: '2', label: '2'},
            {value: '1000', label: '1000'},
            {value: '9000', label: '9000'},
            {value: '400', label: '400'}
        ]
    };

    res.json(sprocs);
});

app.get("/api/vendors", (req, res) => {
    sql.close();
    sql.connect(keys.config)
    .then(() => {
        var request = new sql.Request();
        request.query("select * from Vendors", (err, data) => {
            if (err) console.log(err);
            res.json(data.recordset);
        });
    })
    .catch((err) => {
        console.log(err);
    });
});

app.get("/api/vendor", (req, res) => {
    let name = req.query.name;
    let queryString = "select * from Vendors where Name = '" + name + "'";
    sql.close();
    sql.connect(keys.config)
    .then(() => {
        var request = new sql.Request();
        request.query(queryString, (err, data) => {
            if (err) console.log(err);
            res.json(data.recordset[0])               
        });
    })
    .catch((err) => {
        console.log(err);
    });
});

app.post("/api/edit", urlencodedParser, (req, res) => {
    var name = req.body.template;
    var intSproc = req.body.intSprocs;
    var sfSprocs = req.body.sfSprocs;
    var custID = req.body.customerId;
    var cost = req.body.cost;
    var accountName = req.body.accountName;
    var integrationId = req.body.integrationId?req.body.integrationId:null;
    var type = req.body.type?req.body.type:"";
    var instructions = req.body.instructions;
    var edit = (req.body.edit == "true");
    let query = "select * from Vendors where Name = '"+name+"'";
    sql.close();
    sql.connect(keys.config)
    .then(() => {
        var request = new sql.Request();
        request.query(query, (err, data) => {
            if (err) console.log(err);
            if (edit) {
                let queryString = "update Vendors set Name = '"+name+"', SFSproc = '"+sfSprocs+"', INSproc = '"+intSproc+"', custID = '"+custID+"', cost = '"+cost+"', accountName = '"+accountName+"', vendorIntID = "+integrationId+", integrationType = '"+type+"', instructions = '"+instructions+"' where Name = '"+name+"'";
                request.query(queryString)
                .then((result) =>{
                    console.log("Edited A Vendor");
                })
                .catch((err)=>{
                    console.log(err);
                });
            }
            else if (data.recordset.length === 0) {
                request.query("insert into Vendors (accountID, IntegrationID, Name, SFSproc, INSproc, custID, cost, accountName, vendorIntID, integrationType, deleted, instructions) values (null, null, '"+name+"', "+sfSprocs+", "+intSproc+", "+custID+", "+cost+", '"+accountName+"', "+integrationId+", '"+type+"', 0, '"+instructions+"')")
                .then((result) => {
                    console.log("Added A Vendor");
                })
                .catch((err) => {
                    console.log(err)
                });
            }
            else {
                console.log('Duplicate detected. Cannot insert new vendor');
            }
        });
    });
    res.redirect('/admin/list');
});

app.post('/api/check', urlencodedParser, (req, res) => {
    let template = req.body.template;
    let query = "select * from Vendors where Name = '"+template+"'";
    sql.close();
    sql.connect(keys.config)
    .then(() => {
        let request = new sql.Request();
        request.query(query, (err, data) => {
            if (err) console.log(err);
            console.log(data);
        });
    }); 
});


app.get("/api/delete", (req, res) => {
    let name = req.query.name;
    let queryString = "update Vendors set deleted = 1 where Name = '" + name + "'";
    sql.close();
    sql.connect(keys.config)
    .then(() => {
        var request = new sql.Request();
        request.query(queryString)
        .then(() => {
            res.redirect('/admin/list');
        })
        .catch((err) => {
            console.log(err);
        });
    });
});

app.post('/api/summary', (req, res) => {
    req.pipe(req.busboy);
    req.busboy.on('file', function(fieldname, file, filename){
        console.log("Uploading: " + filename);
        console.log(file);
    });
});

var PORT = 5000;
app.listen(PORT);