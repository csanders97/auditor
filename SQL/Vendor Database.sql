use Auditor;

Create Table Vendors (
	accountID int,
	IntegrationID int,
	Name varchar(255),
	SFSproc int NOT NULL,
	INSproc int NOT NULL,
	custID int NOT NULL,
	cost int NOT NULL,
	accountName varchar(255) NOT NULL,
	vendorIntID int,
	integrationType varchar(255),
	deleted int NOT NULL,
	instructions varchar(max)
);

drop Table Vendors

select * from Vendors

insert into Vendors (accountID, IntegrationID, Name, SFSproc, INSproc, custID, cost, accountName, vendorIntID, integrationType, deleted, instructions)
values (44, null, 'Memes', 9000, 1500, 12, 12, 'New Account Name', 1, 'temp' , 0 , 'Do not delete');

insert into Vendors (accountID, IntegrationID, Name, SFSproc, INSproc, custID, cost, accountName, vendorIntID, integrationType, deleted, instructions)
values (100, 25, 'Dodge', 2, 1000, 12, 12, 'New Account Name', 1, 'temp', 0, 'Do not delete');

-- insert into Vendors (accountID, IntegrationID, Name, SFSproc, INSproc, deleted, billSetup, instructions)
-- values (null, 333, 'Ford', 88, 400, 0, 'Whatever', 'Do not delete');