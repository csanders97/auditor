import React from 'react';
import ViewVendors from './Vendor_List';
import Enzyme, { shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

Enzyme.configure({adapter:new Adapter()});

describe('ViewVendors', () =>{
    var mock = new MockAdapter(axios);
    const mockdata =  
        [
            {"accountID":0,"IntegrationID":0,"Name":"Test","SFSproc":0,"INSproc":0,"deleted":0,"billSetup":0,"instructions":"This is a test vendor"}
        ];
    mock.onGet("/api/vendors").reply(200,mockdata);
    let comp = shallow(<ViewVendors />);
    
    it('renders the vendors list page', () =>{
        expect(comp.find("#vendorsHeader"));
    });
    describe('receives the correct data', () =>{
        it('should get correct data from server', async () =>{
            expect(comp.state().testData).toEqual(mockdata);
        });
        it('currentDataset should only contain a subset of data rows', async () =>{
            expect(comp.state().currentDataset).toEqual(mockdata.slice(0, comp.state().endingPoint));
        });
        it('should get correct accountID', async () =>{
            expect(comp.state().testData.accountID).toEqual(mockdata.accountID);
        });
        it('should get correct IntegrationID', async () =>{
            expect(comp.state().testData.IntegrationID).toEqual(mockdata.IntegrationID);
        });
        it('should get correct Name', async () =>{
            expect(comp.state().testData.Name).toEqual(mockdata.Name);
        });
        it('should get correct SFSproc', async () =>{
            expect(comp.state().testData.SFSproc).toEqual(mockdata.SFSproc);
        });
        it('should get correct INSproc', async () =>{
            expect(comp.state().testData.INSproc).toEqual(mockdata.INSproc);
        });
        it('should get correct billSetup', async () =>{
            expect(comp.state().testData.billSetup).toEqual(mockdata.billSetup);
        });
        it('should get correct instructions', async () =>{
            expect(comp.state().testData.instructions).toEqual(mockdata.instructions);
        });
    });
    describe('renders the vendors list table', () =>{
        it('renders the vendors list table container', () =>{
            expect(comp.find("#Vendor-table"));
        });
        it('renders add vendor button', () =>{
            expect(comp.find("#Add-Vendor-Button"));
        }); 
        describe('renders row correctly', () =>{
            it('renders a row for the vendor list table', () =>{
                expect(comp.find("tr"));
            });
            it('renders accountID field', () =>{
                expect(comp.find("#Row-accountID"));
            }); 
            it('renders IntegrationID field', () =>{
                expect(comp.find("#Row-IntegrationID"));
            }); 
            it('renders Name field', () =>{
                expect(comp.find("#Row-Name"));
            }); 
            it('renders SFSproc field', () =>{
                expect(comp.find("#Row-SFSproc"));
            }); 
            it('renders INSproc field', () =>{
                expect(comp.find("#Row-INSproc"));
            }); 
            it('renders billSetup field', () =>{
                expect(comp.find("#Row-billSetup"));
            }); 
            it('renders instructions field', () =>{
                expect(comp.find("#Row_instructions"));
            }); 
            it('renders edit button', () =>{
                expect(comp.find(".glyphicon-pencil"));
            }); 
            it('renders delete button', () =>{
                expect(comp.find(".glyphicon-trash"));
            }); 
        });
    });
    describe('renders deletion modal properly', () =>
        {
            it('renders modal container', () =>{
                expect(comp.find(".modal"));
            });  
            it('renders modal content', () =>{
                expect(comp.find(".modal-content"));
            }); 
            it('renders modal header', () =>{
                expect(comp.find(".modal-header"));
            }); 
            it('renders modal title', () =>{
                expect(comp.find(".modal-title"));
            }); 
            it('renders modal body', () =>{
                expect(comp.find(".modal-body"));
            }); 
            it('renders modal footer', () =>{
                expect(comp.find(".modal-footer"));
            }); 
            it('renders modal yes button', () =>{
                expect(comp.find("#Modal-yes"));
            }); 
            it('renders modal no button', () =>{
                expect(comp.find("#Modal-no"));
            }); 
        });
});