import React from 'react';
import axios from 'axios';
import { CSVLink } from 'react-csv';
import Collapsible from 'react-collapsible';

class Summary extends React.Component {
    state = {
        testData: []
    };
    list = {
        errorList: []
    };

    componentDidMount = () =>{
        let URL = window.location.href;
        let endURL = URL.substring(URL.lastIndexOf("?"));
        if (endURL.includes("name")) {
            axios('/api/vendor'+endURL)
            .then(response => {
                this.setState({ testData: response.data });
                console.log(this.state.testData);
                console.log(response.data);
            });
        }
    };
    render() {
        return (
            <div>
                <div id="summaryDiv">
                    <h1 id="summaryHeader">Summary of Audit for: {this.state.testData.Name}</h1>
                    <CSVLink className="btn btn-primary" data = {[this.state.testData]}>Download Report</CSVLink>
                </div>
                <br />
                <div id="Info">
                        {                          
                            <ul id="Auditee" key={this.state.testData.Name}>
                                <h2><li>Number of Accounts Audited: 12</li></h2>
                                <h2><li>Number of Duplicate Accounts: 444</li></h2>
                                <h2><li>Number of Potentially Broken Integration: 1</li></h2>
                                <h2><li>Number of Integrations to Cancel: 32</li></h2>
                                <h2><li>Number of Integrations to Audit Further: 44</li></h2>
                            </ul>
                        }
                        {
                            
                            <ul id="Money" key={this.state.testData}>
                                <h2><li>$</li></h2>
                                <h2><li>$</li></h2>
                                <h2><li>$</li></h2>
                                <h2><li>$</li></h2>
                                <h2><li>$</li></h2>
                            </ul>
                        }
                        {
                            <h2 id="Savings">Expected Cost-Savings: $</h2>
                        }
                </div>
            </div>
        );
    }
}

export default Summary;