import React from 'react';
import CreateVendor from './Alter_Vendor';
import Enzyme, { shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

Enzyme.configure({ adapter: new Adapter() });

describe('CreateVendor', () => {
    var mock = new MockAdapter(axios);
    const data = 
        {
            "salesForce": [
                {value: '0', label: '0'},
                {value: '1500', label: '1500'},
                {value: '25', label: '25'},
                {value: '333', label: '333'},
                {value: '2', label: '2'},
                {value: '1000', label: '1000'},
                {value: '9000', label: '9000'},
                {value: '400', label: '400'}
            ],
            "integrations": [
                {value: '0', label: '0'},
                {value: '1500', label: '1500'},
                {value: '25', label: '25'},
                {value: '333', label: '333'},
                {value: '2', label: '2'},
                {value: '1000', label: '1000'},
                {value: '9000', label: '9000'},
                {value: '400', label: '400'}
            ],
            "billSetup": [
                {value: '0', label: '0'},
                {value: '1500', label: '1500'},
                {value: '25', label: '25'},
                {value: '333', label: '333'},
                {value: '2', label: '2'},
                {value: '1000', label: '1000'},
                {value: '9000', label: '9000'},
                {value: '400', label: '400'}
            ]
        };
    mock.onGet("/api/sprocs").reply(200, data);
    let component = shallow(<CreateVendor />);

    it('renders the form page', () => {
        let label = <label>Integration Sprocs:</label>;
        expect(component.contains(label)).toBe(true);
    });

    it('renders the actual form', () => {
        expect(component.find('form').exists()).toBe(true);
    });

    describe('form inputs render onto the page', () => {

        it('renders a template name field', () => {
            expect(component.find('#template').length).toEqual(1);
        });

        it('renders a dropdown for integration sprocs', () => {
            expect(component.find('#int-sprocs').length).toEqual(1);
        });

        it('renders a dropdown for salesforce sprocs', () => {
            expect(component.find('#sf-sprocs').length).toEqual(1);
        });
        
        it('renders customer id field', () => {
            expect(component.find('#customer-id').length).toEqual(1);
        });

        it('renders cost field', () => {
            expect(component.find('#cost').length).toEqual(1);
        });

        it('renders account name field', () => {
            expect(component.find('#account-name').length).toEqual(1);
        });

        it('renders vendor integration id field', () => {
            expect(component.find('#integration-id').length).toEqual(1);
        });

        it('renders type field', () => {
            expect(component.find('#type').length).toEqual(1);
        });

        it('renders a text are for vendor instructions', () => {
            expect(component.find('#instructions').length).toEqual(1);
        });
    });

    describe('Form Input Tests', () => {

        describe('Template Name Input Change', () => {
            it('Accepts Passing Input Name', () => {
                component.find('#template').simulate('change', { target: { name: 'template', value: 'Test Name' } });
                expect(component.state('templateFilled')).toBe(true);
            });

            it('Denies Failing Input Name', () => {
                component.find('#template').simulate('change', { target: { name: 'template', value: '' } });
                expect(component.state('templateFilled')).toBe(false);
            });
        });

        describe('Int Sprocs Select Gets Correct Data', () => {
            it('should get correct data from server', async () =>{
                expect(component.state('integrations')).toEqual(data.integrations);
            });
        });

        describe('Salesforce Sprocs Select Gets Correct Data', () => {
            it('should get correct data from server', async () =>{
                expect(component.state('salesForce')).toEqual(data.salesForce);
            });
        });

        describe('Bill Setup Select Gets Correct Data', () => {
            it('should get correct data from server', async () => {
                expect(component.state('setup')).toEqual(data.billSetup);
            });
        });

        describe('Integration Sprocs Select Change', () => {
            it('Accepts Passing Input Name (N/A)', () => {
                component.find('#int-sprocs').simulate('change', { target: {value: 'N/A' } });
                expect(component.state('integrationSproc')).toEqual({"target": {"value": "N/A"}});
            });

            it('Accepts Passing Input Name (One)', () => {
                component.find('#int-sprocs').simulate('change', { target: {value: 'One' } });
                expect(component.state('integrationSproc')).toEqual({"target": {"value": "One"}});
            });

            it('Accepts Passing Input Name (Two)', () => {
                component.find('#int-sprocs').simulate('change', { target: {value: 'Two' } });
                expect(component.state('integrationSproc')).toEqual({"target": {"value": "Two"}});
            });
        });

        describe('SalesForce Sprocs Select Change', () => {
            it('Accepts Passing Input Name (N/A)', () => {
                component.find('#sf-sprocs').simulate('change', { target: {value: 'N/A' } });
                expect(component.state('salesForceSproc')).toEqual({"target": {"value": "N/A"}});
            });

            it('Accepts Passing Input Name (One)', () => {
                component.find('#sf-sprocs').simulate('change', { target: {value: 'One' } });
                expect(component.state('salesForceSproc')).toEqual({"target": {"value": "One"}});
            });

            it('Accepts Passing Input Name (Two)', () => {
                component.find('#sf-sprocs').simulate('change', { target: {value: 'Two' } });
                expect(component.state('salesForceSproc')).toEqual({"target": {"value": "Two"}});
            });
        });

        describe('Customer ID Input Change', () => {
            it('Accepts Customer ID Input Name', () => {
                component.find('#customer-id').simulate('change', { target: { name: 'customerId', value: '1' } });
                expect(component.state('customerIdFilled')).toBe(true);
            });

            it('Denies Failing Customer ID (letters)', () => {
                component.find('#customer-id').simulate('change', { target: { name: 'customerId', value: 'sdsdsdsa' } });
                expect(component.state('customerIdFilled')).toBe(false);
            });

            it('Denies Failing Customer ID (empty)', () => {
                component.find('#customer-id').simulate('change', { target: { name: 'customerId', value: '' } });
                expect(component.state('customerIdFilled')).toBe(false);
            });

            it('Denies Failing Customer ID (dash)', () => {
                component.find('#customer-id').simulate('change', { target: { name: 'customerId', value: '11-22' } });
                expect(component.state('customerIdFilled')).toBe(false);
            });
        });

        describe('Cost Input Change', () => {
            it('Accepts Cost Input Name', () => {
                component.find('#cost').simulate('change', { target: { name: 'cost', value: '1' } });
                expect(component.state('costFilled')).toBe(true);
            });

            it('Denies Failing Cost (letters)', () => {
                component.find('#cost').simulate('change', { target: { name: 'cost', value: 'sdsdsdsa' } });
                expect(component.state('costFilled')).toBe(false);
            });

            it('Denies Failing Cost (empty)', () => {
                component.find('#cost').simulate('change', { target: { name: 'cost', value: '' } });
                expect(component.state('costFilled')).toBe(false);
            });

            it('Denies Failing Cost (dash)', () => {
                component.find('#cost').simulate('change', { target: { name: 'cost', value: '11-22' } });
                expect(component.state('costFilled')).toBe(false);
            });
        });

        describe('Account Name Input Change', () => {
            it('Accepts Passing Account Name', () => {
                component.find('#account-name').simulate('change', { target: { name: 'accountName', value: 'Test Name' } });
                expect(component.state('accountNameFilled')).toBe(true);
            });

            it('Denies Failing Account Name', () => {
                component.find('#account-name').simulate('change', { target: { name: 'accountName', value: '' } });
                expect(component.state('accountNameFilled')).toBe(false);
            });
        });

        describe('Vendor Integration ID Input Change', () => {
            it('Accepts Vendor Integration ID Input Name', () => {
                component.find('#integration-id').simulate('change', { target: { name: 'integrationId', value: '1' } });
                expect(component.state('intIdFilled')).toBe(true);
            });

            it('Denies Failing Vendor Integration ID (letters)', () => {
                component.find('#integration-id').simulate('change', { target: { name: 'integrationId', value: 'sdsdsdsa' } });
                expect(component.state('intIdFilled')).toBe(false);
            });

            it('Denies Failing Vendor Integration ID (empty)', () => {
                component.find('#integration-id').simulate('change', { target: { name: 'integrationId', value: '' } });
                expect(component.state('intIdFilled')).toBe(false);
            });

            it('Denies Failing Vendor Integration ID (dash)', () => {
                component.find('#integration-id').simulate('change', { target: { name: 'integrationId', value: '11-22' } });
                expect(component.state('intIdFilled')).toBe(false);
            });
        });

        describe('Integration Type Input Change', () => {
            it('Accepts Passing Integration Type', () => {
                component.find('#type').simulate('change', { target: { name: 'type', value: 'Test Name' } });
                expect(component.state('typeFilled')).toBe(true);
            });

            it('Denies Failing Integration Type', () => {
                component.find('#type').simulate('change', { target: { name: 'type', value: '' } });
                expect(component.state('typeFilled')).toBe(false);
            });
        });

        describe('Instructions Input Change', () => {
            it('Accepts Passing Input Name', () => {
                component.find('#instructions').simulate('change', { target: { name: 'template', value: 'Test Name' } });
                expect(component.state('instructionsFilled')).toBe(true);
            });

            it('Denies Failing Input Name', () => {
                component.find('#instructions').simulate('change', { target: { name: 'template', value: '' } });
                expect(component.state('instructionsFilled')).toBe(false);
            });
        });
    });
});