import React from 'react';

class Login extends React.Component {
    state = {
        usernameFilled: false,
        usernameValidSize: false,
        passwordFilled: false,
        passwordIsValidSize: false
    };

    handleNameChange = (event) => {
        let usernameIsFilled = (event.target.value !== "" && (isNaN(event.target.value)));
        let usernameIsValidSize = (event.target.value.length <= 15 && event.target.value.length >= 5);
        this.setState({usernameFilled: usernameIsFilled, usernameValidSize: usernameIsValidSize});
    };

    handlePasswordChange = (event) => {
        let passwordIsFilled = (event.target.value !== "");
        let passwordIsValidSize = (event.target.value.length < 20);
        this.setState({passwordFilled: passwordIsFilled, passwordValidSize: passwordIsValidSize});
    };

    render() {
        return (
            <div className="login-section">
                <h1>Login</h1>
                <div className="error-handling">
                    <div className="error-message">{this.state.usernameFilled ? "" : "*Username must not be empty or contain any numbers."}</div>
                    <div className="error-message">{this.state.passwordFilled ? "" : "*Password must not be empty."}</div>
                    <div className="error-message">{this.state.usernameValidSize ? "" : "*Username must be between 5 and 15 characters long."}</div>
                    <div className="error-message">{this.state.passwordValidSize ? "" : "*Password length must be 20 cahracters or less."}</div>
                </div>
                <div className="login-form">
                    <form action={"/api/authenticate"} method="POST">
                        <div className="login-data">
                            <div className="login-name">
                                <label>UserName:</label>
                                <input type="text" id="name" name="username"
                                className="form-control form-control-md"
                                onChange={this.handleNameChange} required />
                            </div>
                            <div className="login-pass">
                                <label>Password:</label>
                                <input type="password" id="pass" name="password"
                                className="form-control form-control-md"
                                onChange={this.handlePasswordChange} required />
                            </div>
                            <div className="login-submit">
                                <input disabled={this.state.usernameFilled&&this.state.passwordFilled&&this.state.usernameValidSize&&this.state.passwordValidSize ? 0 : 1} type="submit" id="login-submit" className="btn btn-primary" value="Submit" />
                            </div>
                        </div>
                    </form>
                    <a href="/register" className="Register-link">No account? Register here</a>
                </div>
            </div>
        )
    }
}

export default Login;