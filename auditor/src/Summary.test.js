import React from 'react';
import Summary from './Summary';
import Enzyme, { shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { CSVLink } from 'react-csv';

Enzyme.configure({ adapter: new Adapter() });

describe('Summary', () => {
    var mock = new MockAdapter(axios);
    const data = {response:true};
    mock.onGet("/summary_test.json").reply(200, data);
    let comp = render(<Summary />);

    let data2 = [{accountID: "2000", integrationID: "12", CompanyName: "Memes"}];

    it('returns correct data', () => {
        let wrapper = shallow(<Summary />)
        let div = <CSVLink data = {data2}>Download Report</CSVLink>;
        expect(wrapper.find(div));
    });

    it('renders the Summary Page', () => {
        expect(comp.find("#summaryHeader"));
    });

    it('renders the Table', () => {
        expect(comp.find('ol'))
    });

    it('renders the dropdown', () => {
        expect(comp.find('#error1')) 
    });  
});