import React from 'react';

class Register extends React.Component {
    state = {
        usernameFilled: false,
        usernameValidSize: false,
        passwordValue: '',
        passwordFilled: false,
        passwordSame: false,
    };

    handleNameChange = (event) => {
        let usernameIsFilled = (event.target.value !== "" && (isNaN(event.target.value)));
        let usernameIsValidSize = (event.target.value.length <= 15 && event.target.value.length >= 5);
        this.setState({usernameFilled: usernameIsFilled, usernameValidSize: usernameIsValidSize});
    };

    handlePasswordChange = (event) => {
        let passwordIsFilled = (event.target.value !== "");
        let passwordIsValidSize = (event.target.value.length < 20);
        this.setState({passwordFilled: passwordIsFilled, passwordValidSize: passwordIsValidSize, passwordValue: event.target.value});
    };

    handleConfirmChange = (event) => {
        let passwordIsTheSame = true;
        if (event.target.value !== this.state.passwordValue) {
            passwordIsTheSame = false;
        }
        this.setState({passwordSame: passwordIsTheSame});
    }

    render() {
        return (
            <div className="register-section">
                <h1>Register</h1>
                <div className="error-handling">
                    <div className="error-message">{this.state.usernameFilled ? "" : "*Username must not be empty or contain any numbers."}</div>
                    <div className="error-message">{this.state.passwordFilled ? "" : "*Password must not be empty."}</div>
                    <div className="error-message">{this.state.usernameValidSize ? "" : "*Username must be between 5 and 15 characters long."}</div>
                    <div className="error-message">{this.state.passwordValidSize ? "" : "*Password must be shorter than 20 characters."}</div>
                    <div className="error-message">{this.state.passwordSame ? "" : "*Passwords must be the same."}</div>
                </div>
                <div className="register-form">
                    <form action={"/api/registered"} method="POST">
                        <div className="register-data">
                            <div className="register-name">
                                <label>UserName:</label>
                                <input type="text" id="name" name="username"
                                className="form-control form-control-md"
                                onChange={this.handleNameChange} required />
                            </div>
                            <div className="register-pass">
                                <label>Password:</label>
                                <input type="password" id="pass" name="password"
                                className="form-control form-control-md"
                                onChange={this.handlePasswordChange} required />
                            </div>
                            <div className="register-confirm">
                                <label>Confrim your Password:</label>
                                <input type="password" id="confirm" name="confirm"
                                className="form-control form-control-md"
                                onChange={this.handleConfirmChange} required />
                            </div>
                            <div className="register-submit">
                                <input disabled={this.state.usernameFilled&&this.state.passwordFilled&&this.state.usernameValidSize&&this.state.passwordValidSize&&this.state.passwordSame ? 0 : 1} type="submit" id="register-submit" className="btn btn-primary" value="Submit" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default Register;