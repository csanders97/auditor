import React from 'react';
import axios from 'axios';
import Select from 'react-virtualized-select';
import createFilterOptions from 'react-select-fast-filter-options';
import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
import Dropzone from 'react-dropzone';
import request from 'superagent';

class HomePage extends React.Component {
    state = {
        filesPreview:[],
        testData: [],
        uploadedFiles: [],
        printcount: 10,
        vendor: null
    };

    componentDidMount = () => {
        axios('/api/vendors')
        .then(response => {
            let data = response.data;
            data = data.filter((data) => {
                return data.deleted === 0;
            });
            this.setState({
                testData: data
            });
        });
    };


    onDrop = (files) => {
        const req = request.post('/api/summary?name=' + this.state.vendor.Name);
        files.forEach(file => {
            req.attach(file.name, file);
        });
        req.end();
        window.location = '/summary?name=' + this.state.vendor.Name;
    }

    handleVendorSelection = (vendor) => {
        this.setState({ vendor });
        if(vendor == null){
            document.getElementById("Dropbox").style.display = 'none';
        }else{
            document.getElementById("Dropbox").style.display = 'block';
        }
    };

    render() {

        let { vendor } = this.state;

        return (
            <div className="App">
                <div className="Spacer"></div>
                <h1 id="homeHeader">Home</h1>
                <div className="Spacer"></div>                
                <div className="Search">
                    <div className="search-drop-down">
                        <Select
                            className="selectpicker"
                            name="vendor"
                            placeholder="Choose a vendor..."
                            value={vendor}
                            onChange={this.handleVendorSelection}
                            labelKey='Name'
                            options={this.state.testData}
                            filterOptions={this.state.testData}
                            required
                        />
                    </div>
                </div>
                <span className="badge progress-bar-info" id="Vendor-display"></span>
                <Dropzone id={"Dropbox"} onDrop={(files) => this.onDrop(files, this.state.vendor.Name)}>
                    <span className="glyphicon glyphicon-upload"></span>
                    <p>Drag and drop a Bill Here to Upload.</p>
                    <button className="btn btn-primary">Or Select bill to Upload.</button>
                </Dropzone>
                <div className="preview-files">
                    {this.state.filesPreview}
                </div>
            </div>
        );
    }
}

export default HomePage;