import React from 'react';
import Select from 'react-virtualized-select';
import createFilterOptions from 'react-select-fast-filter-options';
import axios from 'axios';
import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';

class CreateVendor extends React.Component {
    state = {
        templateFilled: null,
        customerIdFilled: null,
        costFilled: null,
        accountNameFilled: null,
        intIdFilled: null,
        typeFilled: null,
        integrationSproc: null,
        salesForceSproc: null,
        instructionsFilled: null,
        data: null,
        instructions: "",
        edit: false,
        salesForce: [],
        integrations: [],
    };

    componentDidMount = () => {
        let URL = window.location.href;
        let endURL = URL.substring(URL.lastIndexOf("/"));
        if (endURL.includes("name")) {
            axios('/api'+endURL)
            .then(response => {
                let instruct = response.data.instructions?response.data.instructions:"";
                let vendId = response.data.vendorIntID?response.data.vendorIntID:"";
                let intType = response.data.integrationType?response.data.integrationType:"";
                this.setState({
                    data: response.data,
                    integrationSproc: response.data.INSproc, 
                    salesForceSproc: response.data.SFSproc,
                    vendorIntID: vendId,
                    integrationID: intType,
                    instructions: instruct,
                    edit: true
                });
            });
        }

        axios.get('/api/sprocs')
        .then(results => {
            this.setState({
               salesForce: results.data.salesForce,
               integrations: results.data.integrations,
               setup: results.data.billSetup
            })
        });
    };

    handleNameChange = (event) => {
        let templateIsFilled = true;
        if (event.target.value === "" || (!isNaN(event.target.value))) {
            templateIsFilled = false;
        }
        this.setState({templateFilled: templateIsFilled});
    };

    handleCustomerIdChange = (event) => {
        let idIsNum = true;
        if (isNaN(event.target.value) || event.target.value === "" || event.target.value.includes("-")){
            idIsNum = false;
        }
        this.setState({customerIdFilled: idIsNum});
    }

    handleCostChange = (event) => {
        let costIsNum = true;
        if (isNaN(event.target.value) || event.target.value === "" || event.target.value.includes("-")){
            costIsNum = false;
        }
        this.setState({costFilled: costIsNum});
    }

    handleAccountNameChange = (event) => {
        let nameIsFilled = true;
        if (event.target.value === "" || (!isNaN(event.target.value))) {
            nameIsFilled = false;
        }
        this.setState({accountNameFilled: nameIsFilled});
    }

    handleIntegrationId = (event) => {
        let intIsFilled = true;
        if (isNaN(event.target.value) || event.target.value === "" || event.target.value.includes("-")){
            intIsFilled = false;
        }
        this.setState({intIdFilled: intIsFilled});
    };

    handleTypeChange = (event) => {
        let typeIsFilled = true;
        if (event.target.value === "" || (!isNaN(event.target.value))) {
            typeIsFilled = false;
        }
        this.setState({typeFilled: typeIsFilled});
    };

    handleIntSelection = (integrationSproc) => {
        this.setState({ integrationSproc });
    };

    handleSFSelection = (salesForceSproc) => {
        this.setState({ salesForceSproc });
    }

    handleInstructionsChange = (event) => {
        let instructionsIsFilled = true;
        if (event.target.value === "" || (!isNaN(event.target.value))) {
            instructionsIsFilled = false;
        }
        this.setState({instructionsFilled: instructionsIsFilled, instructions: event.target.value});
    };

    render() {
        let { integrationSproc, salesForceSproc } = this.state;
        let data = this.state.data;
        return (
            <div>
                <form action={"/api/edit"} method="POST">
                    <div className="vendor-data">
                        <div className="template-name">
                            <label>Template:</label>
                            <input type="text" id="template" name="template" defaultValue={data?data.Name:""}
                                    className="form-control form-control-md"
                                    placeholder="Enter Template Name"
                                    onChange={this.handleNameChange} required />
                        </div>
                        <input id="edit" name="edit" value={this.state.edit}/>
                        <div className="sprocs">
                            <div className="int-sprocs">
                                <label>Integration Sprocs:</label>
                                <Select
                                    id="int-sprocs"
                                    name="intSprocs"
                                    placeholder="Choose an Integration Sproc..."
                                    value={integrationSproc}
                                    onChange={this.handleIntSelection}
                                    options={this.state.integrations}
                                    // filterOptions={filterOptions}
                                    required
                                />
                            </div>
                            <div className="sf-sprocs">
                                <label>Salesforce Sprocs:</label>
                                <Select
                                    id="sf-sprocs"
                                    name="sfSprocs"
                                    placeholder="Choose a Salesforce Sproc..."
                                    value={salesForceSproc}
                                    onChange={this.handleSFSelection}
                                    options={this.state.salesForce}
                                    // filterOptions={filterOptions}
                                    required
                                />
                            </div>
                        </div>
                        <div className="setup-required">
                            <div className="customer-id">
                                <label>Customer ID:</label>
                                <input type="text" id="customer-id" name="customerId"
                                    className="form-control form-control-md" defaultValue={data?data.custID:""}
                                    placeholder="Enter Vendor Customer ID"
                                    onChange={this.handleCustomerIdChange} required />
                            </div>
                            <div className="cost-field">
                                <label>Cost:</label>
                                <input type="text" id="cost" name="cost" defaultValue={data?data.cost:""}
                                    className="form-control form-control-md"
                                    placeholder="Enter Vendor Customer ID"
                                    onChange={this.handleCostChange} required />
                            </div>
                            <div className="account-name">
                                <label>Account Name:</label>
                                <input type="text" id="account-name" name="accountName"
                                    className="form-control form-control-md" defaultValue={data?data.accountName:""}
                                    placeholder="Enter Vendor Customer ID"
                                    onChange={this.handleAccountNameChange} required />
                            </div>
                        </div>
                        <div className="setup-other">
                            <div className="integration-id">
                                <label>Vendor Integration ID (Optional):</label>
                                <input type="text" id="integration-id" name="integrationId" defaultValue={this.state.vendorIntID}
                                    className="form-control form-control-md"
                                    placeholder="Enter Vendor Integration ID"
                                    onChange={this.handleIntegrationId} />
                            </div>
                            <div className="type-field">
                                <label>Integration Type (Optional):</label>
                                <input type="text" id="type" name="type" defaultValue={this.state.integrationID}
                                    className="form-control form-control-md"
                                    placeholder="Enter Integration Type"
                                    onChange={this.handleTypeChange} />
                            </div>
                        </div>
                        <div className="instructions form-group">
                            <label htmlFor="instructions">Instructions</label>
                            <textarea className="form-control rounded-2" id="instructions" rows="8" value={this.state.instructions} name="instructions"
                            placeholder="Enter Instructions Here"
                            onChange={this.handleInstructionsChange} required></textarea>
                        </div>
                    </div>
                    <div className="vendor-submit">
                        <button type="submit" id="vendor-submit" className="btn btn-primary" value="Submit">Submit</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default CreateVendor;