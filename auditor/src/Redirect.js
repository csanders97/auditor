import React from 'react';

class Redirect extends React.Component {

    componentDidMount = () =>{
        setTimeout(function(){
            window.location="/";
        }, 5000);
    }

    renderContent() {
        if(!this.props.auth){
            return <div><hr/><p><a href="/login">Click here to login</a></p><p><a href="/register">Click here to register</a></p></div>
        }
    }

    render() {
        return (
            <div>
                <div className="Spacer"></div>
                <div className="Center">
                    <p>You do not have permission to view this page or the page does not exist.</p>
                    <p>Redirecting to home in 5 seconds...</p>
                    {this.renderContent()}
                </div>
            </div>
        )
    }
}

export default Redirect;