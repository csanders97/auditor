import React from 'react';
import HomePage from './Home';
import Enzyme, { shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

Enzyme.configure({ adapter: new Adapter() });

describe('HomePage', () => {
    var mock = new MockAdapter(axios);
    const data = 
        [
            {'INSproc':1500,'IntegrationID':null,'Name':"Memes",'SFSproc':9000,'accountID':44,'billSetup':"333",'deleted':1,'instructions':"Edit Data"}
        ];
    mock.onGet('/api/vendors').reply(200, data);
    let component = shallow(<HomePage />)

    it('Renders the Home Page', () => {
        let header = <h1 id="homeHeader">Home</h1>
        expect(component.contains(header)).toEqual(true);
    });
});