import React from 'react';
import axios from 'axios';

class ViewVendors extends React.Component {
    state = {
        testData: [],
        filteredData: [],
        scrollIncrement: 10,
        endingPoint: 10,
        currentDataset: [],
        endOfDataset: 0,
        deletedName: "",
    };

    componentDidMount = () =>{
        axios('/api/vendors')
        .then(response => {
            let data = response.data;
            data = data.filter((data) => {
                return data.deleted === 0;
            });
            this.setState({
                testData: data, 
                filteredData: data,
                currentDataset: data.slice(0, this.state.endingPoint),
                endOfDataset: this.state.endingPoint === this.state.filteredData.length || this.state.filteredData.length < this.state.scrollIncrement
            });
        });
    };

    findRecord = (event) => {
        let recentKey = event.target.value[event.target.value.length-1];
        if (!(event.target.value === '' || recentKey.match(/([\w\d])/))) {
            event.target.value = ((event.target.value+='').substr(0, event.target.value.length-1));
        };

        let filteredList = this.state.testData;
        filteredList = filteredList.filter((item) => {
            let name = item.Name || "";
            let id = item.accountID || ""; 
            return name.toLowerCase().search(event.target.value.toLowerCase()) !== -1 ||
            id.toString().search(event.target.value) !== -1;
        });
        this.setState({
            filteredData: filteredList,
            currentDataset: filteredList.slice(0, this.state.endingPoint+=this.state.scrollIncrement),
        });
    };

    handleScroll = (event) => {
        if(event.target.scrollHeight - event.target.scrollTop - event.target.clientHeight < 1){
            this.setState({
                endPoint: this.state.endPoint+=this.state.scrollIncrement,
                currentDataset: this.state.filteredData.slice(0, this.state.endingPoint+=this.state.scrollIncrement),
                endOfDataset: this.state.endingPoint >= this.state.filteredData.length
            });
        }
    };

    setDeleted = (event) => {
        this.setState({deletedName: event.target.id});
    }

    render() {
        return (
            <div>
                <div id="Vendor-list" className="Center">
                <div className="Spacer"></div>
                    <h1 id="vendorsHeader">Vendor List</h1>
                    <div className="Spacer"></div>
                    <div className="Vendor-search">
                        <form className="active-cyan-4 mb-4" onSubmit={e => { e.preventDefault(); }}>
                            <input className="form-control" type="text" id="search" aria-label="Search"
                            onChange={this.findRecord}
                            placeholder="Search..." required />
                        </form>
                    </div>
                    <div className="Spacer"></div>
                    <div id="Vendor-table" className="Center Scrollable" onScroll={this.handleScroll}>
                        <table className="table table-sm table-striped">
                            <thead>
                                <tr>
                                    <th scope="col" className="Center Sticky bg-primary">Account ID</th>
                                    <th scope="col" className="Center Sticky bg-primary">Integration ID</th>
                                    <th scope="col" className="Center Sticky bg-primary">Name</th>
                                    <th scope="col" className="Center Sticky bg-primary">Salesforce Sproc</th>
                                    <th scope="col" className="Center Sticky bg-primary">Integration Sproc</th>
                                    <th scope="col" className="Center Sticky bg-primary">Bill Setup</th>
                                    <th scope="col" className="Center Sticky bg-primary">Instructions</th>
                                    <th scope="col" className="Center Sticky bg-primary"><span className="glyphicon glyphicon-pencil"></span></th>
                                    <th scope="col" className="Center Sticky bg-primary"><span className="glyphicon glyphicon-trash"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.currentDataset.map((data, index) => 
                                        <tr key={index}>
                                            <td id="Row-accountID"><b>{data.accountID}</b></td>
                                            <td id="Row-IntegrationID">{data.IntegrationID}</td>
                                            <td id="Row-Name">{data.Name}</td>
                                            <td id="Row-SFSproc">{data.SFSproc}</td>
                                            <td id="Row-INSproc">{data.INSproc}</td>
                                            <td id="Row-billSetup">{data.billSetup}</td>
                                            <td id="Row-instructions">{data.instructions}</td>
                                            <td><a href={"/admin/vendor?name="+data.Name}><button className="btn btn-success" data-toggle="tooltip" title="Edit this user"><span className="glyphicon glyphicon-pencil"></span></button></a></td>
                                            <td><button className="btn btn-danger glyphicon glyphicon-trash" data-toggle="modal" data-target="#Confirmation-modal" id={data.Name} title="Delete this user" onClick={this.setDeleted}></button></td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                        {this.state.endOfDataset?"":<div className="data-loader">Loading more data...</div>}
                        {this.state.currentDataset.length == 0?<div className="data-loader">No data found...</div>:""}
                    </div>
                    <a href="/admin/vendor"><button id="Add-Vendor-Button" className="btn bg-primary"><span className="glyphicon glyphicon-plus"></span> Add Vendor</button></a>
                    <a href="/home"><button id="Add-Vendor-Button" className="btn bg-info">Back</button></a>
                </div>

                <div className="modal" data-backdrop="false" id="Confirmation-modal" role="dialog">
                    <div className="modal-dialog modal-sm">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                                <h4 className="modal-title">Deleting Vendor</h4>
                            </div>
                            <div className="modal-body">
                                <p>Are you sure that you want to delete the vendor "{this.state.deletedName}"?</p>
                            </div>
                            <div className="modal-footer">
                                <a href={"/api/delete?name="+this.state.deletedName}><button type="button" className="btn btn-danger" id="Modal-yes">Yes</button></a>
                                <button type="button" autoFocus="true" className="btn btn-success" data-dismiss="modal" id="Modal-no">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ViewVendors;