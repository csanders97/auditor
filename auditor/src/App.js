import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import * as actions from './actions';

import HomePage from './Home';
import Login from './Login';
import Register from './Register';
import CreateVendor from './Alter_Vendor';
import ViewVendors from './Vendor_List';
import Summary from './Summary';
import Header from './Header';
import Redir from './Redirect';

class App extends Component {
  componentDidMount() {
    this.props.fetchUser();
  }

  renderDirectories(){
    switch(this.props.auth){
      case null:
        return null
      case false:
        return <div> 
          <Switch>
            <Route exact path="/register" render={()=><Register />} />
            <Route exact path="/" render={()=><Login />} />
            <Route exact path="/login" render={()=><Login />} />
            <Route path="/redirect" render={()=><Redirect />}/>
            <Route exact path="*"><Redir to='/redirect' /></Route>
          </Switch>
        </div>
      default:
        return <div>
          <Switch>
            <Route exact path="/" render={()=><HomePage />} />
            <Route exact path="/home" render={()=><HomePage />} />
            <Route path="/admin/vendor" render={()=>this.props.auth.Level === "Admin"?<CreateVendor />:<Redir />} />
            <Route path="/admin/list" render={()=>this.props.auth.Level === "Admin"?<ViewVendors />:<Redir />} />
            <Route path="/summary" render={()=><Summary />} />
            <Route path="/redirect" render={()=><Redir />}/>
            <Route exact path="*"><Redir to='/redirect' /></Route>
          </Switch>
        </div>
    }
  };

  render() {
    return (
      <div className="container">
        <Header />
        <div className="container-center">
          <BrowserRouter>
            <div>
              {this.renderDirectories()}
            </div>
          </BrowserRouter>
          </div>
      </div>
    );
  }
}

function mapStateToProps({auth}){
  return{ auth };
}


export default connect(mapStateToProps, actions)(App);