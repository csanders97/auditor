import React from 'react';
import Register from './Register';
import Enzyme, { shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe('Login', () => {
    let component = shallow(<Register />);

    it('renders the form page', () => {
        let header = <h1>Register</h1>;
        expect(component.contains(header)).toBe(true);
    });

    it('renders the actual form', () => {
        expect(component.find('form').exists()).toBe(true);
    });

    describe('form inputs render onto the page', () => {

        it('renders a username field', () => {
            expect(component.find('#name').length).toEqual(1);
        });

        it('renders a password field', () => {
            expect(component.find('#pass').length).toEqual(1);
        });

        it('renders a password field', () => {
            expect(component.find('#confirm').length).toEqual(1);
        });
    });

    describe('Form Input Tests', () => {
        
        describe('Username Input Change', () => {
            it('Accepts Passing Username', () => {
                component.find('#name').simulate('change', { target: { name: 'username', value: 'CSanders' } });
                expect(component.state('usernameFilled')).toBe(true);
            });

            it('Denies Failing Username', () => {
                component.find('#name').simulate('change', { target: { name: 'username', value: '' } });
                expect(component.state('usernameFilled')).toBe(false);
            });

            it('Denies Short Username', () => {
                component.find('#name').simulate('change', { target: { name: 'username', value: 'test' } });
                expect(component.state('usernameValidSize')).toBe(false);
            });

            it('Denies Long Username', () => {
                component.find('#name').simulate('change', { target: { name: 'username', value: 'testingLongUsernameToo' } });
                expect(component.state('usernameValidSize')).toBe(false);
            });

            it('Accepts Username Within Length', () => {
                component.find('#name').simulate('change', { target: { name: 'username', value: 'perfectSize' } });
                expect(component.state('usernameValidSize')).toBe(true);
            });
        });

        describe('Password Input Change', () => {
            it('Accepts Passing Password', () => {
                component.find('#pass').simulate('change', { target: { name: 'password', value: 'pass' } });
                expect(component.state('passwordFilled')).toBe(true);
            });

            it('Denies Failing Password', () => {
                component.find('#pass').simulate('change', { target: { name: 'password', value: '' } });
                expect(component.state('passwordFilled')).toBe(false);
            });

            it('Denies Short Password', () => {
                component.find('#pass').simulate('change', { target: { name: 'password', value: 'tooLongOfAPasswordThisWillNotPass' } });
                expect(component.state('passwordValidSize')).toBe(false);
            });

            it('Accepts Password Within Length', () => {
                component.find('#pass').simulate('change', { target: { name: 'password', value: 'perfectSize' } });
                expect(component.state('passwordValidSize')).toBe(true);
            });
        });

        describe('Confirm Password Input Change', () => {
            it('Accepts Matching Password', () => {
                component.find('#pass').simulate('change', { target: { name: 'password', value: 'pass' } });
                component.find('#confirm').simulate('change', { target: { name: 'confirm', value: 'pass' } });
                expect(component.state('passwordSame')).toBe(true);
            });
        });
    });
});