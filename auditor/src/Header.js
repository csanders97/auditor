import React from 'react';
import { connect } from 'react-redux';

class Header extends React.Component {
    renderContent() {
        switch(this.props.auth){
            case null:
                return null;
            case false:
                return <div className="Row Logdiv">
                    <a href="/"><button className="btn btn-primary">Login</button></a>
                    <a href="/register"><button className="btn btn-primary">Sign Up</button></a>
                </div>;
            default:
                return <div className="Row Logdiv">
                    <div className="Logstate">Hello, {this.props.auth.UserName}</div>
                    {this.props.auth.Level === "Admin"?<a href="/admin/list"><button className="btn btn-primary" data-toggle="tooltip" title="View/Edit Vendor List">Vendors</button></a>:""}
                    <a href="/api/logout"><button className="btn btn-primary">Logout</button></a>
                </div>;
        }
    }
    render(){
        return (
            <div className="Header">
                <div className="Inner-header">
                    {this.renderContent()}
                    <h1><a href="/">THE AUDITOR</a></h1>
                </div>
                <hr />
            </div>
        )
    }
};

function mapStateToProps({auth}){
    return{ auth };
}

export default connect(mapStateToProps)(Header);